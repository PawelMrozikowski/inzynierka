import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {UserDetailComponent} from './components/userDetail/userDetail.component';
import {LandingPageComponent} from './components/landingPage/landingPage.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {AccountPageComponent} from './components/accountPage/account.component';
import {EditAccountComponent} from './components/editAccount/editAccount.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: 'users', component: LandingPageComponent },
      { path: 'user/:id', component: UserDetailComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'account', component: AccountPageComponent },
      //{ path: 'account/:id', component: AccountPageComponent }
      { path: 'edit/account', component: EditAccountComponent }
      //{ path: 'edit/account/:id', component: EditAccountComponent }
    ]
  }
  // {
  //   path: 'user/:id',
  //   component: UserDetailComponent,
  //   // pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
