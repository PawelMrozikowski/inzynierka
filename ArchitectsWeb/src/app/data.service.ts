import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginForm} from './components/login/login.form';
import {RegisterForm} from './components/register/register.form';
import {UserForm} from './components/accountPage/user.form';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private API_URL_LOCAL = 'http://localhost:8080/';

  private API_URL = this.API_URL_LOCAL;

  private LOGIN_ENDPOINT = 'users/login';
  private REGISTER_ENDPOINT = "users/registration"
  private MY_ACCOUNT_ENDPOINT = "users/me"


  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  static isUserLogin() {
    return localStorage.getItem('currentUserToken') != null;
  }

  static logout() {
    localStorage.clear();
  }

  login(loginForm: LoginForm) {
    return this.http.post(this.API_URL + this.LOGIN_ENDPOINT, loginForm, { observe: 'response' })
      .pipe(
        tap(response => {
          const token = response.headers.get('Authorization');
          localStorage.setItem('currentUserToken', token);
        }),
        catchError(this.handleError<any>('login'))
      );
  }


  register(registerForm: RegisterForm) {
    return this.http.post(this.API_URL + this.REGISTER_ENDPOINT, registerForm, { observe: 'response' })
      .pipe(
        tap(response => {
          const token = response.headers.get('Authorization');
          localStorage.setItem('currentUserToken', token);
        }),
        catchError(this.handleError<any>('register'))
      );
  }

  getMyAccount(): Observable<UserForm> {
    this.httpOptions.headers =
      this.httpOptions.headers.set('Authorization', this.currentToken());
    return this.http.get<UserForm>(this.API_URL + this.MY_ACCOUNT_ENDPOINT, this.httpOptions);
  }

  private currentToken() {
    return localStorage.getItem('currentUserToken');
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
