import {Category} from './Category';

export class User {
  id: number;
  firstName: string;
  lastName: string;
  emailAddress: string;
  webSite: string;
  phoneNumber: string;
  imagePath: string;
  description: string;
  avgRate: number;
  categories: Category[];
  // rates: Rate[];
  // comments: Comment[];
  // portfolio: Portfolio[];
}
