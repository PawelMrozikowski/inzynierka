import {User} from './User';

export class Comment {
  body: string;
  user: User;
}
