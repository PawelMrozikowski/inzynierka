import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {LoginForm} from './login.form';
import {DataService} from '../../data.service';


@Component({
  selector: 'app-login-html',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  constructor(private dataService: DataService, private router: Router) {}

  loginForm: LoginForm = {
    username: '',
    password: ''
  };

  doLogin() {
    this.dataService.login(this.loginForm)
      .subscribe(response => {
        console.log(response);
        this.router.navigate(['account']);
      });
  }

}
