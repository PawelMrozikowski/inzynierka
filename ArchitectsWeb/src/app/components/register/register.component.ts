import {RegisterForm} from './register.form';
import {DataService} from '../../data.service';
import {Router} from '@angular/router';
import {Component} from '@angular/core';

@Component({
  selector: 'app-register-html',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {

  constructor(private dataService: DataService, private router: Router) {}

  registerForm: RegisterForm = {
    username: '',
    password: '',
    passwordConfirm: '',
    emailAddress: '',
    lastName: ''
  };

  register() {
    this.dataService.register(this.registerForm)
      .subscribe(response => {
        console.log(response);
        this.router.navigate(['account']);
      });
  }
}
