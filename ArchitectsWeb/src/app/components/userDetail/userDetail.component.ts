import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../models/User';
import {HttpClient} from '../../../../node_modules/@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {Rate} from '../../models/Rate';
import {Comment} from '../../models/Comment';


@Component({
  selector: 'app-user-detail-html',
  templateUrl: './userDetail.component.html',
  styleUrls: ['./userDetail.component.css']
})


export class UserDetailComponent implements OnInit {

  user: User;
  id: string;
  display: string;
  rate: Rate;
  text: string;

  comment: Comment = {
    body: '',
    user: this.user
  };

  comments: Comment[] = [];

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  showUser(id: number) {
    this.http.get<User>(`http://localhost:8080/users/${id}`)
      .subscribe(data => {
        console.log(data);
        this.user = data;
      });

    console.log(this.user);

    // for(let i = 1; i < this.user.avgRate; i++){
    //   let cur = document.getElementById("rate"+i);
    //   cur.className="fa fa-star checked";
    // }
  }

  addRate(rate: number) {
    this.rate = new Rate();
    this.rate.rate = 5;
    this.rate.user_id = this.user.id;
    this.http.post<Rate>(`http://localhost:8080/rates/add`, this.rate)
      .subscribe(data => {
        console.log(data);
        this.rate = data;
      });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.showUser(+this.id);
    this.showComments(+this.id);
  }

  addComment() {
    this.comment.user = this.user;
    this.http.post<Comment>(`http://localhost:8080/comments/add`, this.comment)
      .subscribe(data => {
        console.log(data);
        this.comment = data;
      });
    console.log(this.user.id);
    this.showComments(this.user.id);
    this.text = '';
  }

  showComments(id: number) {
    this.http.get<Comment[]>(`http://localhost:8080/users/comments/${id}`)
      .subscribe(data => {
        console.log(data);
        this.comments = data;
      });
  }



  add(ths: number, sno: number){
    for (let i=1;i<=5;i++){
      let cur = document.getElementById("star"+i);
      cur.className="fa fa-star";
    }
    for (let i=1;i<=sno;i++){
      let cur=document.getElementById("star"+i);
      if(cur.className=="fa fa-star") {
        cur.className="fa fa-star checked";
      }
    }
  }

  openModal(){
    this.display="block";
  }

  onCloseHandled(){
    this.display="none";
  }
}
