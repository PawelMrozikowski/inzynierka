import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-landing-html',
  templateUrl: './landingPage.component.html',
  styleUrls: ['./landingPage.component.css']
})

export class LandingPageComponent implements OnInit {

  users: User[] = [];

  constructor(private http: HttpClient, private router: Router) {}

  navigate(id: number) {
    this.router.navigate([`user/${id}`]);
  }

  ngOnInit() {
    this.http.get<User[]>('http://localhost:8080/users/all')
      .subscribe( data => {
        this.users = data;
      });
  }

  filterByCategory(categoryName: string) {
    this.http.get<User[]>(`http://localhost:8080/users/filter/${categoryName}`)
      .subscribe(data => {
      console.log(data);
      this.users = data;
    });
  }

  filterByLastName(lastName: string) {
    console.log(lastName);
    this.http.get<User[]>(`http://localhost:8080/users/find/${lastName}`)
      .subscribe(data => {
        console.log(data);
        this.users = data;
      });
  }

  filterByProvince(province: string) {
    console.log(province);
    this.http.get<User[]>(`http://localhost:8080/address/find/${province}`)
      .subscribe(data => {
        this.users = data;
      }, err => {
         this.users = [];
      });

    console.log(this.users);
  }

  sortByRate(rate: string) {
    console.log(rate);
    this.http.get<User[]>(`http://localhost:8080/users/sort/${rate}`)
      .subscribe(data => {
        console.log(data);
        this.users = data;
      });
  }
}
