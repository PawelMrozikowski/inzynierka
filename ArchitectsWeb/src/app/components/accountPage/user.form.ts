export class UserForm {
    username: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    webSite: string;
    phoneNumber: string;
    imagePath: string;
    description: string;
    avgRate: number;
  }