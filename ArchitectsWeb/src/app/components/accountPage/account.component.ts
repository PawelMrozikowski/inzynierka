import {UserForm} from './user.form';
import {DataService} from '../../data.service';
import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-account-html',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})

export class AccountPageComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router)  {}

  me: UserForm = {
    username: '',
    firstName: '',
    lastName: '',
    emailAddress: '',
    webSite: '',
    phoneNumber: '',
    imagePath: '',
    description: '',
    avgRate: 0
  };

  ngOnInit() {
    this.dataService.getMyAccount()
      .subscribe(response => {
        console.log(response);
        this.me = response;
      });
  }

  navigate() {
    this.router.navigate(['edit/account']);
  }
}
