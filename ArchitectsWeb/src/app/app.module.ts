import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {LandingPageComponent} from './components/landingPage/landingPage.component';
import {UserDetailComponent} from './components/userDetail/userDetail.component';
import {RouterModule, Routes} from '@angular/router';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {AccountPageComponent} from './components/accountPage/account.component';
import {EditAccountComponent} from './components/editAccount/editAccount.component';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
  // {
  //   path: '',
  //   component: AppComponent
  // },
  // {
  //   path: 'user/:id',
  //   component: UserDetailComponent
  // }
];

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    UserDetailComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    AccountPageComponent,
    EditAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
