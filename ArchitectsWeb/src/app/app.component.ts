import { Component } from '@angular/core';
import {HttpClient} from '../../node_modules/@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ArchitectsWeb';

  constructor(private router: Router) {
    this.router.navigate([`users`]);
  }
}
