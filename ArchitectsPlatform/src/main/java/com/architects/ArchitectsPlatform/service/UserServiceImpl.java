package com.architects.ArchitectsPlatform.service;

import com.architects.ArchitectsPlatform.dto.EditUserForm;
import com.architects.ArchitectsPlatform.dto.TokenResponse;
import com.architects.ArchitectsPlatform.entity.Rate;
import com.architects.ArchitectsPlatform.entity.User;
import com.architects.ArchitectsPlatform.errors.AppError;
import com.architects.ArchitectsPlatform.repository.RoleRepository;
import com.architects.ArchitectsPlatform.repository.UserRepository;
import com.architects.ArchitectsPlatform.security.LoggedUsernameProvider;
import com.architects.ArchitectsPlatform.security.TokenAuthService;
import com.architects.ArchitectsPlatform.validator.UserValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.vavr.control.Either;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder bCryptPasswordEncoder;
    private LoggedUsernameProvider loggedUsernameProvider;
    private final UserValidator userValidator;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder bCryptPasswordEncoder, LoggedUsernameProvider loggedUsernameProvider, UserValidator userValidator) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.loggedUsernameProvider = loggedUsernameProvider;
        this.userValidator = userValidator;
    }

    @Transactional
    @Override
    public User update(EditUserForm editUserForm) {

        final User user = userRepository.findByUsername(loggedUsernameProvider.provide());

        Optional.ofNullable(editUserForm.getFirstName()).ifPresent(user::setFirstName);
        Optional.ofNullable(editUserForm.getLastName()).ifPresent(user::setLastName);
        Optional.ofNullable(editUserForm.getDescription()).ifPresent(user::setDescription);
        Optional.ofNullable(editUserForm.getEmailAddress()).ifPresent(user::setEmailAddress);
        Optional.ofNullable(editUserForm.getImagePath()).ifPresent(user::setImagePath);
        Optional.ofNullable(editUserForm.getPhoneNumber()).ifPresent(user::setPhoneNumber);
        Optional.ofNullable(editUserForm.getWebSite()).ifPresent(user::setWebSite);

        return userRepository.save(user);
    }

    @Override
    public TokenResponse getTokenFromCurrentUser() throws JsonProcessingException {
        return generateTokenResponse(loggedUsernameProvider.provide());
    }

    @Override
    public Either<AppError, TokenResponse> register(User user) {

        return userValidator.getValidateUserForm(user)
                .map(this::saveValidUser)
                .map(u -> generateTokenResponse(u.getUsername()));
    }

    @Override
    public User getMe() {
        final String username = loggedUsernameProvider.provide();
        return userRepository.findByUsername(username);
    }

    private TokenResponse generateTokenResponse(String username)  {

        String token = null;

        try {
            token = TokenAuthService.generateJWT(username);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return new TokenResponse(token);
    }

    private User saveValidUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        return userRepository.save(user);
    }

    public void calculateAvgRate(User user){
        int sum = 0;
        int avg = 0;
        for(Rate rate : user.getRates()){
            sum += rate.getRate();
        }
        if(user.getRates().size() > 0){
            avg = sum/user.getRates().size();
        }
        user.setAvgRate(avg);
    }
}
