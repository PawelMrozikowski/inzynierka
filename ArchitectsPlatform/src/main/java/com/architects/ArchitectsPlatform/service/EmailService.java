package com.architects.ArchitectsPlatform.service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class EmailService {

    public static void sendEmail(String[] args) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.mail.yahoo.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("julia.lewandowska@yahoo.com","oprst123");
                    }
                });

        try {
            System.out.println("try to send message");
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("julia.lewandowska@yahoo.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("yuliya.levandovskaya@gmail.com"));
            System.out.println("after login");
            message.setSubject("Testing Subject");
            message.setText("Dear Mail Crawler," +
                    "\n\n No spam to my email, please!");
            System.out.println("send message");
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            System.out.println("Error");
            throw new RuntimeException(e);
        }
    }
}
