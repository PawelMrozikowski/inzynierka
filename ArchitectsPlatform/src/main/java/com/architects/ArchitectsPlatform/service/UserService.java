package com.architects.ArchitectsPlatform.service;

import com.architects.ArchitectsPlatform.dto.EditUserForm;
import com.architects.ArchitectsPlatform.dto.TokenResponse;
import com.architects.ArchitectsPlatform.entity.User;
import com.architects.ArchitectsPlatform.errors.AppError;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.vavr.control.Either;

public interface UserService {

    User update(EditUserForm editUserForm);

    TokenResponse getTokenFromCurrentUser() throws JsonProcessingException;

    Either<AppError, TokenResponse> register(User user) throws JsonProcessingException;

    User getMe();
}