package com.architects.ArchitectsPlatform.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static java.util.Collections.emptyList;

public class TokenAuthService {

    private static final long EXPIRATIONTIME = 864_000_000; // 10 days
    private static final String SECRET = "ThisIsASecret";
    private static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static void addAuthToToken(HttpServletResponse res, String username) throws JsonProcessingException {
        String s = generateJWT(username);
        res.addHeader(HEADER_STRING, s);
    }

    static Authentication getAuthFromToken(HttpServletRequest request) throws IOException {
        String username = getUser(request.getHeader(HEADER_STRING));
        return username != null ?
                new UsernamePasswordAuthenticationToken(username, null, emptyList()) :
                null;
    }

    public static String generateJWT(String username) throws JsonProcessingException {
        return TOKEN_PREFIX + " " + Jwts.builder()
                .setSubject(objectMapper.writeValueAsString(username))
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private static String getUser(String token) throws IOException {
        if (token == null) return null;

        String subject = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody()
                .getSubject();

        return objectMapper.readValue(subject, String.class);
    }
}