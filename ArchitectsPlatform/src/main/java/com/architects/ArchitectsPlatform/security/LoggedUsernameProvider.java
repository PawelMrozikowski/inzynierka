package com.architects.ArchitectsPlatform.security;

@FunctionalInterface
public interface LoggedUsernameProvider {

    String provide();

}
