package com.architects.ArchitectsPlatform.errors;

public enum ErrorReason {

    PASSWORD_NOT_CONFIRM,
    FIELD_REQUIRED,
    INVALID_LENGTH_USERNAME,
    USER_EXISTS

}