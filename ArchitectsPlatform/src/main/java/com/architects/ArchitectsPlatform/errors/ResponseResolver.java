package com.architects.ArchitectsPlatform.errors;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Either;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
public class ResponseResolver {

    private final static Map<ErrorReason, HttpStatus> HTTP_STATUS_MAP =
            HashMap.of(
                    ErrorReason.PASSWORD_NOT_CONFIRM, HttpStatus.BAD_REQUEST
            );

    public <T> ResponseEntity<?> resolve(final Either<AppError, T> either) {
        return either
                .map(this::createObject)
                .getOrElseGet(this::createError);
    }

    private ResponseEntity<Object> createObject(final Object object) {
        return new ResponseEntity<>(object, HttpStatus.OK);
    }

    private ResponseEntity<Object> createError(final AppError error) {
        return new ResponseEntity<>(error, HTTP_STATUS_MAP.getOrElse(error.getErrorReason(), HttpStatus.BAD_REQUEST));
    }

}