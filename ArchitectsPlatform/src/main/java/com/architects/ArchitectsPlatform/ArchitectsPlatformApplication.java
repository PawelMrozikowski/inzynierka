package com.architects.ArchitectsPlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchitectsPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArchitectsPlatformApplication.class, args);
		System.out.println("Application started");

//		User user = new User();
//		user.setFirstName("Adam");
//		user.setLastName("Kowalski");
//
//		UserService userService = new UserService();
//		if(user != null){
//			userService.saveUser(user);
//		}

	}
}
