package com.architects.ArchitectsPlatform.repository;

import com.architects.ArchitectsPlatform.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
