package com.architects.ArchitectsPlatform.repository;

import com.architects.ArchitectsPlatform.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
