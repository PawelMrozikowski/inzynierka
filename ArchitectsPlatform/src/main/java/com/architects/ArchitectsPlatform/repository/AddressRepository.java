package com.architects.ArchitectsPlatform.repository;

import com.architects.ArchitectsPlatform.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
    Address findByProvince(String province);

}
