package com.architects.ArchitectsPlatform.repository;

import com.architects.ArchitectsPlatform.entity.Rate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepository extends JpaRepository<Rate, Long> {
}
