package com.architects.ArchitectsPlatform.repository;

import com.architects.ArchitectsPlatform.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findBylastName(String lastName);
    User findByUsername(String username);

}
