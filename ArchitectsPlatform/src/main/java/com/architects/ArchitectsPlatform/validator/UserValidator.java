package com.architects.ArchitectsPlatform.validator;

import com.architects.ArchitectsPlatform.entity.User;
import com.architects.ArchitectsPlatform.errors.AppError;
import com.architects.ArchitectsPlatform.errors.ErrorReason;
import com.architects.ArchitectsPlatform.repository.UserRepository;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserValidator {

    private final UserRepository userRepository;

    public UserValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Either<AppError, User> getValidateUserForm(User user) {

        if(StringUtils.isEmpty(user.getUsername()) || StringUtils.containsWhitespace(user.getUsername())) {
            return Either.left(new AppError(ErrorReason.FIELD_REQUIRED, "This field is required"));
        }

        if(StringUtils.isEmpty(user.getPassword()) || StringUtils.containsWhitespace(user.getPassword())) {
            return Either.left(new AppError(ErrorReason.FIELD_REQUIRED, "This field is required"));
        }

        if(StringUtils.isEmpty(user.getPasswordConfirm()) || StringUtils.containsWhitespace(user.getPasswordConfirm())) {
            return Either.left(new AppError(ErrorReason.FIELD_REQUIRED, "This field is required"));
        }

        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            return Either.left(new AppError(ErrorReason.INVALID_LENGTH_USERNAME, "Please use between 6 and 32 characters"));
        }

        if(userRepository.findByUsername(user.getUsername()) != null) {
            return Either.left(new AppError(ErrorReason.USER_EXISTS, "Someone already has that username"));
        }

        if (!user.getPasswordConfirm().equals(user.getPassword())) {
            return Either.left(new AppError(ErrorReason.PASSWORD_NOT_CONFIRM, "These passwords don't match"));
        }

        return Either.right(user);

    }


}
