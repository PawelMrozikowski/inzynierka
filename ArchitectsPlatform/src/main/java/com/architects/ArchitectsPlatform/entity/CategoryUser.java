//package com.architects.ArchitectsPlatform.entity;
//
//import javax.persistence.*;
//
//@Entity
//public class CategoryUser {
//
//    @Id
//    @GeneratedValue
//    private Long idCategoryUser;
//
//    @ManyToMany
//    @JoinColumn(name="userId")
//    private User user;
//
//    @ManyToMany
//    @JoinColumn(name="categoryId")
//    private Category category;
//
//    public Long getIdCategoryUser() {
//        return idCategoryUser;
//    }
//
//    public void setIdCategoryUser(Long idCategoryUser) {
//        this.idCategoryUser = idCategoryUser;
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//
//    public Category getCategory() {
//        return category;
//    }
//
//    public void setCategory(Category category) {
//        this.category = category;
//    }
//
//    @Override
//    public String toString() {
//        return "CategoryUser{" +
//                "idCategoryUser=" + idCategoryUser +
//                ", user=" + user +
//                ", category=" + category +
//                '}';
//    }
//}
