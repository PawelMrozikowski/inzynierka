package com.architects.ArchitectsPlatform.entity;

import javax.persistence.*;

@Entity
@Table(name="portfolio")
public class Portfolio {

    @Id
    @GeneratedValue
    private Long id;

    private String imagePath;

    public Portfolio() {}

    public Long getPortfolioId() {
        return id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
