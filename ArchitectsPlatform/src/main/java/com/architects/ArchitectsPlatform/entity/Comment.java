package com.architects.ArchitectsPlatform.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="comment")
public class Comment {

    @Id
    @GeneratedValue
    private Long id;
    private String body;
    private LocalDate created_at;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDate getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDate created_at) {
        this.created_at = created_at;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment{" +
                ", body='" + body + '\'' +
                ", created_at=" + created_at +
                '}';
    }
}
