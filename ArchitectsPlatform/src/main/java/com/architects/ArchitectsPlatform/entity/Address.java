package com.architects.ArchitectsPlatform.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="address")
public class Address {

    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String province;
    @Column
    private String city;

    @OneToMany
    @JoinColumn(name="address_id")
    private List<User> users;

    public Address(){}

    public Long getId() {
        return id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
