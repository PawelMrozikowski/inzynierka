package com.architects.ArchitectsPlatform.entity;

import javax.persistence.*;

@Entity
@Table(name="rate")
public class Rate {

    @Id
    @GeneratedValue
    private Long id;
    private Integer rate;

    @Transient
    private Long user_id;

    public Rate(){}

    public Long getIdRate() {
        return id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "id=" + id +
                ", rate=" + rate +
                '}';
    }
}
