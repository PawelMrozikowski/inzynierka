package com.architects.ArchitectsPlatform.controller;

import com.architects.ArchitectsPlatform.entity.Address;
import com.architects.ArchitectsPlatform.entity.Comment;
import com.architects.ArchitectsPlatform.repository.AddressRepository;
import com.architects.ArchitectsPlatform.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    CommentRepository commentRepository;

    @PostMapping("/add")
    public Comment addComment(@RequestBody Comment comment){
        commentRepository.save(comment);
        return comment;
    }

    @GetMapping("/all/{id}")
    public List<Comment> findAllByUserId(){
        return commentRepository.findAll();
    }
}
