package com.architects.ArchitectsPlatform.controller;

import com.architects.ArchitectsPlatform.entity.Category;
import com.architects.ArchitectsPlatform.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @PostMapping("/add")
    public void addCategory(Category category){
        categoryRepository.save(category);
    }

    @GetMapping(value = "/all")
    public List<Category> getAll(){
        return categoryRepository.findAll();
    }
}
