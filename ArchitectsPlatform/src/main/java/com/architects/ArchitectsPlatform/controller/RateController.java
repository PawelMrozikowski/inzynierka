package com.architects.ArchitectsPlatform.controller;

import com.architects.ArchitectsPlatform.entity.Address;
import com.architects.ArchitectsPlatform.entity.Rate;
import com.architects.ArchitectsPlatform.repository.AddressRepository;
import com.architects.ArchitectsPlatform.repository.RateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rates")
public class RateController {
    @Autowired
    RateRepository rateRepository;

    @CrossOrigin
    @PostMapping("/add")
    public Rate addRate(Rate rate){
        rateRepository.save(rate);
        return rate;
    }

    @CrossOrigin
    @GetMapping("/all")
    public List<Rate> findAll(){
        return rateRepository.findAll();
    }
}
