package com.architects.ArchitectsPlatform.controller;

import com.architects.ArchitectsPlatform.dto.EditUserForm;
import com.architects.ArchitectsPlatform.dto.TokenResponse;
import com.architects.ArchitectsPlatform.entity.Category;
import com.architects.ArchitectsPlatform.entity.Comment;
import com.architects.ArchitectsPlatform.entity.Rate;
import com.architects.ArchitectsPlatform.entity.User;
import com.architects.ArchitectsPlatform.errors.ResponseResolver;
import com.architects.ArchitectsPlatform.repository.CategoryRepository;
import com.architects.ArchitectsPlatform.repository.UserRepository;
import com.architects.ArchitectsPlatform.service.UserService;
import com.architects.ArchitectsPlatform.service.UserServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserService userService;

    @Autowired
    ResponseResolver responseResolver;

    @Autowired
    UserServiceImpl userServiceImpl;


    @PostMapping(value = "/registration")
    public ResponseEntity<?> registration(@RequestBody User user) throws JsonProcessingException {
        return responseResolver.resolve(userService.register(user));
    }

    @PostMapping(value = "/login")
    public TokenResponse loginUser() throws JsonProcessingException {
        return userService.getTokenFromCurrentUser();
    }

    @PutMapping("/update")
    public User update(@RequestBody EditUserForm editUserForm) {
        return userService.update(editUserForm);
    }

    @GetMapping("/me")
    public User me() {
        return userService.getMe();
    }

    @GetMapping(value = "/all")
    public List<User> getAll(){
        List<User> users = userRepository.findAll();
//        for(User user : users){
//            userServiceImpl.calculateAvgRate(user);
//        }
        return users;
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") final Long id) {
        Optional<User> user = userRepository.findById(id);
        userServiceImpl.calculateAvgRate(user.get());
        return user.orElse(new User());
    }

    //find by category name
    @GetMapping("/filter/{name}")
    public List<User> getUsersByCategory(@PathVariable("name") final String name) {
        List<User> users = userRepository.findAll();
        List<User> usersToReturn = new LinkedList<>();

        for(User user : users){
            for(Category cat : user.getCategories()){
                if(cat.getName().equals(name)){
                    usersToReturn.add(user);
                }
            }
        }

        return usersToReturn;
    }

    @GetMapping("/comments/{id}")
    public List<Comment> getCommentsByUser(@PathVariable("id") final Long id) {
        User user = userRepository.findById(id).get();
        return user.getComments();
    }

    //find by user last name
    @GetMapping("/find/{lastName}")
    public List<User> getUsersByName(@PathVariable("lastName") final String lastName) {
        List<User> users = userRepository.findBylastName(lastName);
        return users;
    }

    //find by user last name
    @GetMapping("/sort/{rateAvg}")
    public List<User> sortByRate(@PathVariable("rateAvg") final String rateAvg) {
        List<User> users = userRepository.findAll();

        for(User user : users){
            userServiceImpl.calculateAvgRate(user);
        }
        if(rateAvg.equals("najwyzsza")) {
            users.stream().sorted(Comparator.comparing(User::getAvgRate));
        } else if(rateAvg.equals("najnizsza")) {
            users.stream().sorted(Comparator.comparing(User::getAvgRate).reversed());
        }
        return users;
    }

    @PostMapping("/add")
    public void addUser(@RequestBody User user){
        userRepository.save(user);
    }

    @PutMapping("/update/{id}/{name}")
    public User update(@PathVariable("id") final Long id, @PathVariable("name") final String name){
        User user = getUserById(id);
        user.setFirstName(name);
        return userRepository.save(user);
    }
}