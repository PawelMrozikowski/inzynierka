package com.architects.ArchitectsPlatform.controller;

import com.architects.ArchitectsPlatform.entity.Address;
import com.architects.ArchitectsPlatform.entity.User;
import com.architects.ArchitectsPlatform.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    AddressRepository addressRepository;

    @PostMapping("/add")
    public Address addAddress(Address address){
        addressRepository.save(address);

        return address;
    }

    @GetMapping("/all")
    public List<Address> findAll(){
        return addressRepository.findAll();
    }

    //find by user last name
    @CrossOrigin
    @GetMapping("/find/{province}")
    public List<User> getUsersByProvince(@PathVariable("province") final String province) {
        Address addressByProvice = addressRepository.findByProvince(province);
        List<User> users = addressByProvice.getUsers();
        return users;
    }

}
