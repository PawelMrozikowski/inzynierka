package com.architects.ArchitectsPlatform.dto;


public class EditUserForm {

    private String firstName;
    private String lastName;
    private String emailAddress;
    private String webSite;
    private String phoneNumber;
    private String imagePath;
    private String description;
    private String username;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getWebSite() {
        return webSite;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getDescription() {
        return description;
    }

    public String getUsername() {
        return username;
    }
}
