package com.architects.ArchitectsPlatform.dto;

public class LoginForm {

    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
